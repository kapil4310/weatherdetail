import React, { useState, useEffect } from "react";
import "./App.css";
import Weather from "./Weather";
const APIKEY = "0764df93791340daad065bc1f4bc92af";
//var weatherObjects = [];
function App() {
  const [weather, setWeather] = useState([]);
  const [city, setCity] = useState("");
  const onInputChange = (e) => {
    setCity(e.target.value);
  };
  const onRemoveClick = (index) => {
    weather.splice(index, 1);
    setWeather([...weather]);
  };
  async function fetchDataByCity() {
    await fetch(
      `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${APIKEY}`
    )
      .then((res) => res.json())
      .then((data) => {
        setWeather([...weather, data]);
        setCity(" ");
      });
  }

  useEffect(() => {
    fetch(
      `http://api.openweathermap.org/data/2.5/find?lat=55.5&lon=37.5&cnt=4&appid=${APIKEY}`,
      {
        method: "GET",
      }
    )
      .then((res) => res.json())
      .then((data) => setWeather(data.list));
  }, []);
  return (
    <div className="App">
      <h3>Weather Detail</h3>
      <div className="horizontal-list">
        {weather.map((i, index) => {
          {
            console.log(i, index);
          }
          return (
            <Weather
              key={i.id}
              city={i.name}
              temperature={Math.round((i.main.temp * 9) / 5 - 459.67)}
              description={i.weather[0].description}
              country={i.sys.country}
              onRemoveClick={() => onRemoveClick(index)}
            />
          );
        })}
      </div>
      <div className="inputAndSubmit">
        <input
          className="inputstyle"
          type="text"
          value={city}
          onChange={onInputChange}
        />
        <button
          type="button"
          className="btn btn-outline-success btn-sm buttonstyle"
          onClick={fetchDataByCity}
        >
          Add
        </button>
      </div>
    </div>
  );
}

export default App;
