import React from "react";
import "./Weather.css";
const URL = "http://openweathermap.org/img/wn/";
const WeatherDesPlusPcitures = {
  "clear sky": "01d.png",
  "few clouds": "02d.png",
  "scattered clouds": "03d.png",
  "broken clouds": "04d.png",
  "shower rain": "09d.png",
  rain: "10d.png",
  thunderstorm: "11d.png",
  snow: "13d.png",
  mist: "50d.png",
  "overcast clouds": "02d.png",
};

const Weather = ({
  city,
  temperature,
  description,
  country,
  onRemoveClick,
}) => {
  return (
    <div className="tile">
      <div>
        {city && country && (
          <p>
            {city},{country}
          </p>
        )}
      </div>
      <div>
        <img
          className="imageStyle"
          src={URL + WeatherDesPlusPcitures[description]}
        />
      </div>
      <div className="infoRow">
        <div className="infoTemp">
          {temperature && <p>{temperature} &#8457;</p>}
        </div>
        <div className="infoText">{description && <p>{description}</p>}</div>
      </div>
      <button
        className="remove-button btn btn-outline-danger "
        onClick={onRemoveClick}
      >
        Remove
      </button>
    </div>
  );
};
export default Weather;
